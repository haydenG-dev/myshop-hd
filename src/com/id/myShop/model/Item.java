package com.id.myShop.model;

public class Item {
	
	String name; 
	int id; 
	float price;
	int nbrElet; 
	
	
	
	
	public Item() {
		super();
		// TODO Auto-generated constructor stub
	}




	public Item(String name, int id, float price, int nbrElet) {
		super();
		this.name = name;
		this.id = id;
		this.price = price;
		this.nbrElet = nbrElet;
	}




	@Override
	public String toString() {
		return "Item [name=" + name + ", id=" + id + ", price=" + price + ", nbrElet=" + nbrElet + "]";
	}




	public String getName() {
		return name;
	}




	public void setName(String name) {
		this.name = name;
	}




	public int getId() {
		return id;
	}




	public void setId(int id) {
		this.id = id;
	}




	public float getPrice() {
		return price;
	}




	public void setPrice(float price) {
		this.price = price;
	}




	public int getNbrElet() {
		return nbrElet;
	}




	public void setNbrElet(int nbrElet) {
		this.nbrElet = nbrElet;
	}




	String display() {
		return name;
		
	}
	

}
